from django.conf.urls import patterns, include, url
from rabinovichtv import settings
from django.contrib import admin
from main.feed import LatestNewsFeed, LatestVideoFeed

from haystack.query import SearchQuerySet
from haystack.views import SearchView

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'main.views.main', name='main'),
    url(r'^about', 'main.views.main', name='about'),
    url(r'^contacts', 'main.views.main', name='contacts'),

    #Photo news
    url(r'^photo/$', 'main.views.view_photo_news_all', name='view_photo_news_all'),

    #News
    url(r'^news/$', 'main.views.view_news_all', name='view_news_all'),
    url(r'^news/(?P<news_slug>[^\.]+).html', 'main.views.view_news', name='view_news'),

    #About and static
    url(r'^channel/about-us/$', 'main.views.main'),
    url(r'^channel/broadcasting/$', 'main.views.main'),
    url(r'^channel/news/$', 'main.views.main'),
    url(r'^channel/programs/$', 'main.views.main'),
    url(r'^channel/team/$', 'main.views.main'),
    url(r'^channel/feedback/$', 'main.views.feedback', name='feedback'),
    url(r'^channel/feedback/thankyou/$', 'main.views.feedback_thankyou', name='feedback_thankyou'),


    #Video
    url(r'^video/(?P<category_slug>[^\.]+)/(?P<video_slug>[^\.]+).html', 'main.views.view_video', name='view_video'),
    url(r'^video/(?P<category_slug>[^\.]+)/$', 'main.views.view_video_category', name='view_video_category'),
    url(r'^video/$', 'main.views.view_video_all', name='view_video_all'),

    #Poll
    url(r'^polls/(?P<poll_id>\d+)/vote/$', 'main.views.polls_vote', name='polls_vote'),
    url(r'^polls/(?P<poll_id>\d+)/result/$', 'main.views.polls_result', name='polls_result'),

    #RSS
    url(r'^rss/news/$', 'main.views.rss_news'),
    url(r'^rss/video/$', 'main.views.rss_video'),
    url(r'^feed/news$', LatestNewsFeed(), name='rss_news'),
    url(r'^feed/video$', LatestVideoFeed(), name='rss_videos'),

    url(r'^admin/', include(admin.site.urls)),

    (r'^tinymce/', include('tinymce.urls')),
    (r'^mce_filebrowser/', include('mce_filebrowser.urls')),


)


#Search
sqs = SearchQuerySet().order_by('-date')
urlpatterns = patterns('haystack.views',
    url(r'^search/$', SearchView(
        searchqueryset=sqs,
    )),
) +urlpatterns


if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
) + urlpatterns