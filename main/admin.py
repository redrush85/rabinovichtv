from django.contrib import admin
from django_admin_bootstrapped.admin.models import SortableInline
from sorl.thumbnail.admin import AdminImageMixin
from mce_filebrowser.admin import MCEFilebrowserAdmin
from main.models import *

class NewsGalleryImageInline(AdminImageMixin, admin.TabularInline):
    fieldsets = (
        (
            None,
            {
                'fields': ('image', 'title')
            }
        ),
    )

    model = NewsGalleryImage
    extra = 0


class NewsAdmin(AdminImageMixin, MCEFilebrowserAdmin):
    list_display = ('title', 'preview', 'is_photo', 'title_color', 'views_count', 'is_main_page', 'is_active', 'date')
    search_fields = ('title',)
    list_filter = ('title_color', 'is_photo', 'is_active', 'is_main_page', 'date')
    ordering = ('-date',)
    list_per_page = 20
    inlines = (NewsGalleryImageInline, )
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title',)
    ordering = ('id',)
    prepopulated_fields = {'slug': ('title',)}


class VideoAdmin(AdminImageMixin, MCEFilebrowserAdmin):
    list_display = ('title', 'preview', 'category', 'views_count', 'date', 'is_main_page', 'is_active')
    ordering = ('-date',)
    list_filter = ('category', 'date', 'is_active', 'is_main_page')
    list_per_page = 20
    search_fields = ['title']
    date_hierarchy = 'date'
    prepopulated_fields = {'slug': ('title',)}

class StaticPageAdmin(MCEFilebrowserAdmin):
    list_display = ('title', 'template', 'is_active')
    ordering = ('-id',)
    prepopulated_fields = {'slug': ('title',)}


class MenuItemInlineAdmin(admin.TabularInline, SortableInline):

    model = MenuItem
    extra = 0


class MenuAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_per_page = 30
    inlines = (MenuItemInlineAdmin,)


class SurveyVariantsInline(admin.TabularInline):
    fieldsets = (
        (
            None,
            {
                'fields': ('title', 'count')
            }
        ),
    )

    model = SurveyVariants
    extra = 0


class SurveyAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_active', 'date')
    search_fields = ('title',)
    list_filter = ('is_active', 'date')
    ordering = ('-date',)
    list_per_page = 20
    inlines = (SurveyVariantsInline, )


class MarqueeAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_active')
    search_fields = ('title',)
    list_filter = ('is_active',)
    ordering = ('-id',)
    list_per_page = 20


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('subject', 'title', 'date')
    list_filter = ('date', )
    date_hierarchy = 'date'
    ordering = ('-date',)
    list_per_page = 20



# Register your models here.
admin.site.register(StaticPage, StaticPageAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(Survey, SurveyAdmin)
admin.site.register(Marquee, MarqueeAdmin)
