from django import template
from django.template.defaultfilters import stringfilter
from main.youtube import get_video_id
register = template.Library()




@register.filter(name='youtube_embed_url')
# converts youtube URL into embed HTML
# value is url
def youtube_embed_url(value):
    video_id = get_video_id(value)
    if video_id:
        embed_url = 'http://www.youtube.com/embed/%s' %video_id
        res = "<iframe width=\"656\" height=\"404\" src=\"%s\" frameborder=\"0\" allowfullscreen></iframe>" %(embed_url)
        return res
    return None

youtube_embed_url.is_safe = True


@register.filter(name='youtube_main_embed_url')
def youtube_main_embed_url(value):
    video_id = get_video_id(value)
    if video_id:
        embed_url = 'http://www.youtube.com/embed/%s' %video_id
        res = "<iframe width=\"654\" height=\"360\" src=\"%s\" frameborder=\"0\" allowfullscreen></iframe>" %(embed_url)
        return res
    return ''

youtube_main_embed_url.is_safe = True


@stringfilter
def youthumbnail(value, args):
    video_id = get_video_id(value)
    if video_id:
        if args == 's':
            return "http://img.youtube.com/vi/%s/2.jpg" % video_id
        elif args == 'l':
            return "http://img.youtube.com/vi/%s/0.jpg" % video_id
        else:
            return None
    else:
        return None

register.filter('youthumbnail', youthumbnail)