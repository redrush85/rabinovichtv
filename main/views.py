# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from main.models import *
from datetime import datetime
from main.forms import FeedbackForm
from django.core.paginator import EmptyPage, PageNotAnInteger
from main.paginator import FlynsarmyPage, FlynsarmyPaginator
from django.utils import feedgenerator
from django.utils.html import strip_tags
from django.views.decorators.cache import cache_page
from rabinovichtv import settings

# Create your views here.
@cache_page(60 * settings.__CACHE_TIME__)
def main(request):

    top_news = News.objects.filter(is_main_page=True, is_active=True, date__lte=datetime.now()).order_by('-date')[:6]
    last_videos = Video.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:16]
    top_video = Video.objects.filter(is_active=True, is_main_page=True, date__lte=datetime.now()).order_by('-date').first()

    return render_to_response('index.html', {"top_news": top_news, "last_videos": last_videos, "top_video": top_video}, context_instance=RequestContext(request))


@cache_page(60 * settings.__CACHE_TIME__)
def view_video(request, category_slug, video_slug):
    video = get_object_or_404(Video, slug=video_slug, is_active=True)
    video.views_count+=1
    video.save()
    return render_to_response('video.html', {
        'video': video},
        context_instance=RequestContext(request)
    )


@cache_page(60 * settings.__CACHE_TIME__)
def view_video_category(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    videos_list = Video.objects.filter(category=category, is_active=True, date__lte=datetime.now()).order_by('-date')
    paginator = FlynsarmyPaginator(videos_list, 21, adjacent_pages=6) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        videos = paginator.page(page)
    except PageNotAnInteger:
        videos = paginator.page(1)
    except EmptyPage:
        videos = paginator.page(paginator.num_pages)

    return render_to_response("category.html", {"category": category, "videos": videos}, context_instance=RequestContext(request))


@cache_page(60 * settings.__CACHE_TIME__)
def view_video_all(request):
    videos_list = Video.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')
    paginator = FlynsarmyPaginator(videos_list, 14, adjacent_pages=6) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        videos = paginator.page(page)
    except PageNotAnInteger:
        videos = paginator.page(1)
    except EmptyPage:
        videos = paginator.page(paginator.num_pages)

    return render_to_response("video_all.html", {"videos": videos}, context_instance=RequestContext(request))


@cache_page(60 * settings.__CACHE_TIME__)
def view_news_all(request):
    news_list = News.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')
    paginator = FlynsarmyPaginator(news_list, 14, adjacent_pages=6) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)


    return render_to_response("news_all.html", {"news": news}, context_instance=RequestContext(request))



@cache_page(60 * settings.__CACHE_TIME__)
def view_photo_news_all(request):
    news_list = News.objects.filter(is_active=True, is_photo=True, date__lte=datetime.now()).order_by('-date')
    paginator = FlynsarmyPaginator(news_list, 14, adjacent_pages=6) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        news = paginator.page(page)
    except PageNotAnInteger:
        news = paginator.page(1)
    except EmptyPage:
        news = paginator.page(paginator.num_pages)


    return render_to_response("photo_news_all.html", {"news": news}, context_instance=RequestContext(request))


def polls_vote(request, poll_id):
    poll_question = get_object_or_404(Survey, pk=poll_id, is_active=True)
    choices = poll_question.surveyvariants_set.all()

    #If User voted
    voted_polls=request.session.get('has_voted_poll',[])
    if poll_id in voted_polls:
        total_votes = sum(c.count for c in choices)

        for choice in choices:
            vote = choice.count
            vote_percentage = int(vote*100.0/total_votes)
            choice.percentage = vote_percentage

        return render_to_response('polls_vote.html', {
            'poll_question': poll_question,
            'choices': choices
        }, context_instance=RequestContext(request))


    try:
        selected_choice = poll_question.surveyvariants_set.get(pk=request.POST['poll_id_answer'])
    except (KeyError, SurveyVariants.DoesNotExist):
        # Redisplay the poll voting form.
        return render_to_response('polls_get.html', {
            'poll_question': poll_question,
            'poll_answers': choices,
            'error_message': u"Вы не сделали ваш выбор",
        }, context_instance=RequestContext(request))
    else:
        selected_choice.count += 1
        selected_choice.save()
        request.session['has_voted_poll']=voted_polls+[poll_id]

        total_votes = sum(c.count for c in choices)

        for choice in choices:
            vote = choice.count
            vote_percentage = int(vote*100.0/total_votes)
            choice.percentage = vote_percentage

        return render_to_response('polls_vote.html', {
            'poll_question': poll_question,
            'choices': choices
        }, context_instance=RequestContext(request))


def polls_result(request, poll_id):
    poll_question = get_object_or_404(Survey, pk=poll_id)
    choices = poll_question.surveyvariants_set.all()
    total_votes = sum(c.count for c in choices)

    for choice in choices:
        vote = choice.count
        vote_percentage = int(vote*100.0/total_votes)
        choice.percentage = vote_percentage

    return render_to_response('polls_vote.html', locals(), context_instance=RequestContext(request))


@cache_page(60 * settings.__CACHE_TIME__)
def view_news(request, news_slug):
    article = get_object_or_404(News, slug=news_slug, is_active=True)
    article.views_count+=1
    article.save()
    return render_to_response('article.html', {
        'article': article},
        context_instance=RequestContext(request)
    )


def feedback(request):
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("feedback_thankyou"))
    else:
        form = FeedbackForm()
    return render_to_response('feedback.html', {'form': form}, context_instance=RequestContext(request) )


def feedback_thankyou(request):
    return render_to_response('feedback_thankyou.html', context_instance=RequestContext(request) )


def format(str):
    str = str.replace('>', '>\n')
    str = str.replace('<', '\n<')
    str = str.replace('\n\n', '\n')
    return str


@cache_page(60 * 1)
def rss_news(request):

    feed = feedgenerator.Rss201rev2Feed(
        title=u"Rabinovich-TV News",
        link=u"http://rabinovich.tv",
        description=u"",
        language=u"ru",
        )

    news = News.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:20]

    for news_item in news:
        feed.add_item(
            title=news_item.title,
            link='http://'+request.META['HTTP_HOST']+news_item.get_absolute_url(),
            description=strip_tags(news_item.content)
        )

    str = feed.writeString('utf-8')
    str = format(str)
    return HttpResponse(str)


@cache_page(60 * settings.__CACHE_TIME__)
def rss_video(request):


    feed = feedgenerator.Rss201rev2Feed(
        title=u"Rabinovich-TV Video",
        link=u"http://rabinovich.tv",
        description=u"",
        language=u"ru",
        )

    videos = Video.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:20]

    for video_item in videos:
        feed.add_item(
            title=video_item.title,
            link='http://'+request.META['HTTP_HOST']+video_item.get_absolute_url(),
            description=strip_tags(video_item.content)
        )

    str = feed.writeString('utf-8')
    str = format(str)
    return HttpResponse(str)