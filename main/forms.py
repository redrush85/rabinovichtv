# -*- coding: utf-8 -*-
from django.forms import ModelForm, TextInput, Textarea
from main.models import Feedback


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback

    widgets = {
            'title': TextInput(attrs={'class': 'field'}),
            'email': TextInput(attrs={'class': 'field'}),
            'subject': TextInput(attrs={'class': 'field'}),
            'body': TextInput(attrs={'class': 'message'}),
        }

    fields = ['title', 'email', 'subject', 'body']
