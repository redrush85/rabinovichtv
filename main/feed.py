# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.utils.html import strip_tags
from main.models import News, Video
from datetime import datetime

class LatestNewsFeed(Feed):
    title = "Rabinovich-TV News"
    link = "http://rabinovich.tv"
    description = ""

    def items(self):
        return News.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:20]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return strip_tags(item.content)

    def item_pubdate(self, item):
        return item.date

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return item.get_absolute_url()


class LatestVideoFeed(Feed):
    title = "Rabinovich-TV Video"
    link = "http://rabinovich.tv"
    description = ""

    def items(self):
        return Video.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:20]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return strip_tags(item.content)

    def item_pubdate(self, item):
        return item.date

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return item.get_absolute_url()