# -*- coding: utf-8 -*-
import datetime
from haystack import indexes
from main.models import News, Video


class NewsIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    date = indexes.DateTimeField(model_attr=u'date')
    is_active = indexes.BooleanField(model_attr=u'is_active')

    def get_model(self):
        return News

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(is_active=True, date__lte=datetime.datetime.now())


class VideoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    date = indexes.DateTimeField(model_attr=u'date')
    is_active = indexes.BooleanField(model_attr=u'is_active')

    def get_model(self):
        return Video

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(is_active=True, date__lte=datetime.datetime.now())
