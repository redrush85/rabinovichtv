# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from sorl.thumbnail import ImageField
from sorl.thumbnail import get_thumbnail
from tinymce.models import HTMLField
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from main.youtube import get_video_id

colors = (
    ('black', u'Обычный'),
    ('black_bold', u'Обычный жирный'),
    ('red', u'Красный'),
    ('red_bold', u'Красный жирный'),
    ('yellow', u'Желтый'),
    ('yellow_bold', u'Желтый жирный')
)


class Menu(models.Model):
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Меню"
        verbose_name_plural = u"Меню"



class MenuItem(models.Model):
    menu = models.ForeignKey(Menu)
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')
    url = models.CharField(max_length=255, verbose_name=u'URL')
    is_active = models.BooleanField(default=True, verbose_name=u'Опубликована')
    #order = models.IntegerField(default=0, verbose_name=u'Порядок сортировки')
    position = models.PositiveSmallIntegerField(default=0, verbose_name=u'Порядок сортировки')

    class Meta:
        ordering = ('position',)
        verbose_name = u"Пункт меню"
        verbose_name_plural = u"Меню"



class StaticPage(models.Model):
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')
    content = HTMLField(blank=True, verbose_name=u'Содержимое')
    template = models.FilePathField(path=settings.TEMPLATE_DIRS[0]+'/static/', verbose_name=u'Шаблон')
    slug = models.SlugField(max_length=100, verbose_name="URL", unique=True)
    is_active = models.BooleanField(default=True, verbose_name=u'Опубликована')

    class Meta:
        verbose_name = u"Статическая страница"
        verbose_name_plural = u"Статические страницы"

    def __unicode__(self):
        return self.title



class Category(models.Model):
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')
    #is_mainmenu = models.BooleanField(default=True, verbose_name=u'В главном меню')
    slug = models.SlugField(max_length=100, verbose_name="URL", unique=True)

    class Meta:
        verbose_name = u"Категория"
        verbose_name_plural = u"Категории"

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('view_video_category', (), {
            'category_slug': self.slug,
    })



class News(models.Model):
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')
    content = HTMLField(verbose_name=u'Содержимое')
    image = ImageField(max_length=255, blank=True, upload_to='news', verbose_name=u'Изображение')
    title_color = models.CharField(max_length=100, choices=colors, default='black', verbose_name=u'Цвет заголовка')
    is_photo = models.BooleanField(default=False, verbose_name=u'Фоторепортаж')
    views_count = models.IntegerField(default=0, verbose_name=u'Количество просмотров')
    slug = models.SlugField(max_length=100, verbose_name="URL", unique=True)
    is_main_page = models.BooleanField(default=False, verbose_name=u'На главной')
    is_active = models.BooleanField(default=True, verbose_name=u'Опубликована')
    date = models.DateTimeField(verbose_name=u'Дата публикации')

    class Meta:
        verbose_name = u"Новость"
        verbose_name_plural = u"Новости"

    def __unicode__(self):
        return self.title


    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True

    @models.permalink
    def get_absolute_url(self):
        return ('view_news', (), {
            'news_slug': self.slug,
    })


class NewsGalleryImage(models.Model):
    image = ImageField(upload_to="news_gallery", verbose_name=u'Изображение')
    title = models.CharField(max_length=300, blank=True, verbose_name=u'Заголовок')
    news = models.ForeignKey(News)

    def __unicode__(self):
        return self.title


    class Meta:
        verbose_name = u"Изображение"
        verbose_name_plural = u"Изображения галереи"

    def preview(self):
        if self.image:
            im = get_thumbnail(self.image, '100x100', crop='center')
            return '<img src="%s">' % im.url
        return ''

    preview.allow_tags = True



class Video(models.Model):
    title = models.CharField(max_length=200,verbose_name=u'Заголовок')
    category = models.ForeignKey(Category, related_name = 'category', verbose_name=u'Категория')
    content = HTMLField(blank=True, verbose_name=u'Описание')
    youtube_link = models.CharField(max_length=200, verbose_name=u"Ссылка на Youtube")
    views_count = models.IntegerField(default=0, verbose_name=u'Количество просмотров')
    date = models.DateTimeField(verbose_name=u'Дата публикации')
    is_main_page = models.BooleanField(default=False, verbose_name=u'На главной')
    is_active = models.BooleanField(default=True, verbose_name=u'Опубликовано')
    slug = models.SlugField(max_length=200, verbose_name="URL", unique=True)


    class Meta:
        verbose_name = u"Видеоролик"
        verbose_name_plural = u"Видеоролики"

    def __unicode__(self):
        return self.title

    def clean(self):
        # Check valid Youtube link
        if self.youtube_link:
            video_id = get_video_id(self.youtube_link)
            if not video_id:
                raise ValidationError(u'Проверьте правильность URL видеоролика!')


    def preview(self):
        if self.youtube_link:
            video_id = get_video_id(self.youtube_link)
            if video_id:
                return "<img src='http://img.youtube.com/vi/%s/2.jpg'/>" % video_id
            else:
                return ''
        return ''
    preview.allow_tags = True


    @models.permalink
    def get_absolute_url(self):
        return ('view_video', (), {
            'category_slug': self.category.slug,
            'video_slug': self.slug,
    })


class Survey(models.Model):
    title = models.CharField(max_length=200,verbose_name=u'Вопрос голосования')
    is_active = models.BooleanField(default=True, verbose_name=u'Активен')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата')

    class Meta:
        verbose_name = u"Опрос"
        verbose_name_plural = u"Опросы"

    def __unicode__(self):
        return self.title


class SurveyVariants(models.Model):
    title = models.CharField(max_length=200,verbose_name=u'Вариант ответа')
    count = models.IntegerField(default=0, verbose_name=u'Количество голосов')
    survey = models.ForeignKey(Survey)

    class Meta:
        verbose_name = u"Вариант ответа"
        verbose_name_plural = u"Варианты ответа"

    def __unicode__(self):
        return self.title


class Marquee(models.Model):
    title = models.CharField(max_length=400, verbose_name=u'Заголовок')
    is_active = models.BooleanField(default=True, verbose_name=u'Опубликовано')

    class Meta:
        verbose_name = u"Бегущая строка"
        verbose_name_plural = u"Бегущая строка"

    def __unicode__(self):
        return self.title



class Feedback(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Имя')
    email = models.EmailField(max_length=100, verbose_name=u'E-Mail')
    subject = models.CharField(max_length=400, blank=True, verbose_name=u'Тема')
    body = models.TextField(max_length=4000, verbose_name=u'Сообщение')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u"Обратная связь"
        verbose_name_plural = u"Обратная связь"