#-*- coding: utf-8 -*-
from main.models import MenuItem, Menu, Marquee, SurveyVariants, Survey, News
from datetime import datetime


def menu(request):
    try:
        parentmenu = Menu.objects.get(title=u"Главное меню")
    except:
        return ''
    if parentmenu:
        menu = MenuItem.objects.filter(menu=parentmenu, is_active=True).order_by('position')
    return locals()

def marquee_text(request):
    marquee_text = Marquee.objects.filter(is_active=True)
    return locals()

def news_block(request):
    news_block = News.objects.filter(is_active=True, date__lte=datetime.now()).order_by('-date')[:10]
    return locals()

def poll(request):
    poll_voted = False
    poll_question = Survey.objects.filter(is_active=True).order_by('-id').first()
    if not poll_question:
        return ''
    poll_answers = poll_question.surveyvariants_set.all()

    #If user voted
    voted_polls=request.session.get('has_voted_poll',[])
    if str(poll_question.id) in voted_polls:
        poll_voted = True

        total_votes = sum(c.count for c in poll_answers)

        for choice in poll_answers:
            vote = choice.count
            vote_percentage = int(vote*100.0/total_votes)
            choice.percentage = vote_percentage

    return locals()


